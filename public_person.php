<?php
class Person{
    public $name;

    public function setName($person_name){
        $this->name=$person_name;
    }

    public function getName(){
       return $this->name;
    }
}

class Actor extends  Person{

}

class Student extends Person{
    public $studentID;
    public function setStudentID($studentID){
       $this->studentID=$studentID;
    }

    public function studentInfo(){
        $this->name="Amir Khan";
        echo $this->name."<br>";
        echo $this->studentID."<br>";

        parent::setName("Faisal Korim");
        echo parent::getName()."<br>";

    }


}


class Food{
    public function testMethod(){
        $objPerson=new Person();
        $objStudent=new Student();

        $objPerson->setName("Food Person");
        echo $objPerson->getName()."<br>";

        $objStudent->setName("Food Student");
        echo $objStudent->getName()."<br>";


        $objStudent->studentInfo();

    }

}
$objFood=new Food();
$objFood->testMethod();

$person=new Person();
$person->setName("Salman Khan");
echo $person->getName()."<br>";

$student=new Student();
$student->setStudentID("SEIP136509");
$student->studentInfo();



?>



